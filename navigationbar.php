<style>
.navbar{
  margin-bottom: 5vh;
}
  .navbar-nav > li{
    padding-left:1vw;
    padding-right: 1vw;
  }

  .navbar-brand{
    padding-right: 4vw;
  }

  .ml-auto{
      padding-right: 6vw;
  }
</style>


<nav class="navbar navbar-expand-md navbar-dark bg-primary">
  <a class="navbar-brand" href="manageproduct.php">Inventory Management</a>
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="manageproduct.php">Manage Products</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="managetransaction.php">Place Order</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="viewtransaction.php">View Transactions</a>
            </li>
            
        </ul>
    </div>
    <!--<div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="#">Navbar 2</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div> -->
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Options
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Document Tracking</a>
                  <a class="dropdown-item" href="#">Inventory Management</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Logout</a>
                </div>
            </li>
            
        </ul>
    </div>
</nav>