<?php session_start(); 
include("connection.php");
?>

<!DOCTYPE html>
<html>
<head>
	<style>
	
		#header{font-size: 1.5em;}
		
		.myprice{
			margin-left:3vw;
			margin-bottom:4vh; 

		}
		.myquantity{
			margin-left:3vw;
			margin-bottom:4vh; 
		}
		.myproduct{
			margin-left:3vw;
			margin-bottom:4vh; 
		}
	</style>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script type="text/javascript">
    	function calculatePrice(){
    		var myform = document.getElementById('myform');
    		var myprice = document.getElementsByClassName('myprice');
    		var myquantity = document.getElementsByClassName('myquantity');
    		var sum=0;
    		//window.alert(myControls.toSource());
			for (var i = 0; i < myprice.length; i++) {
			   var aPrice = parseInt(myprice[i].value);
			   var aQuantity = parseInt(myquantity[i].value);

			   sum = sum + aPrice;
			    
			}
		    document.getElementById('header').innerHTML = "Total amount : <i class='fa fa-rupee'></i>"+sum;

		}	


    function isValidDate(str){
	// STRING FORMAT yyyy-mm-dd
	if(str=="" || str==null){return false;}								
	
	// m[1] is year 'YYYY' * m[2] is month 'MM' * m[3] is day 'DD'					
	var m = str.match(/(\d{4})-(\d{2})-(\d{2})/);
	
	// STR IS NOT FIT m IS NOT OBJECT
	if( m === null || typeof m !== 'object'){return false;}				
	
	// CHECK m TYPE
	if (typeof m !== 'object' && m !== null && m.size!==3){return false;}
				
	var ret = true; //RETURN VALUE						
	var thisYear = new Date().getFullYear(); //YEAR NOW
	var minYear = 1999; //MIN YEAR
	
	// YEAR CHECK
	if( (m[1].length < 4) || m[1] < minYear || m[1] > thisYear){ret = false;}
	// MONTH CHECK			
	if( (m[2].length < 2) || m[2] < 1 || m[2] > 12){ret = false;}
	// DAY CHECK
	if( (m[3].length < 2) || m[3] < 1 || m[3] > 31){ret = false;}
	
	return ret;			
} 


	   function doValidation(){
        	var vendor_name = document.getElementById('vendor_name').value;
        	var order_date = document.getElementById('order_date').value;
        	if(!isValidDate(order_date)){
        		window.alert("enter valid date");
        		return false;
        	}
        	var discount = document.getElementById('discount').value;
        	if(vendor_name.length == 0 || order_date.length==0 || discount.length==0 ){ 
        		window.alert('please enter all input fields1');
                return false;
        	}
        	if(isNaN(discount)){
					window.alert("Please enter number in the input field");			   		
			   		 return false;
			   }
        	
        	var myprice = document.getElementsByClassName('myprice');
    		var myquantity = document.getElementsByClassName('myquantity');

			for (var i = 0; i < myprice.length; i++) {
			   var aPrice = myprice[i].value;   
			   var aQuantity = myquantity[i].value;
			   if(aPrice.length ==0 || aQuantity.length==0){
                     window.alert('please enter all input fields2');
                     return false;
			   }
			   if(isNaN(aPrice) || isNaN(aQuantity)){
					window.alert("Please enter number in the input field");			   		
			   		 return false;
			   }
			   
			    
			}
			return true;
        }
			 
         

		

       function addFields(){
            var number = document.getElementById("member").value;
            if(isNaN(number)){
            	window.alert("number of products must be an integer");
            	return false;
            } 
            if(number.length == 0){
            	window.alert("please enter number of products");
            	return false;
            }
            var container = document.getElementById("container");
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
            for (i=0;i<number;i++){
	            	//For product name
	            	<?php  
	                	$arr = array(); $i=0;
	                	$sql = "select * from product";
	                	$resultset = mysqli_query($conn, $sql);
	                	while($row = mysqli_fetch_array($resultset)){
	                		$arr[$i] = $row[1];
	                		$i++;
	                	}
	                ?>

	                var pause = [];
	                <?php foreach($arr as $key => $val){ ?>
				        pause.push('<?php echo $val; ?>');
				    <?php } ?>
					//Create array of options to be added
					
					container.appendChild(document.createTextNode("Product " + (i+1)));
					//Create and append select list
					var selectList = document.createElement("select");
					selectList.id = "mySelect";
					selectList.setAttribute('name', 'product[]');
					selectList.setAttribute('id', 'product[]');
					selectList.setAttribute('class', 'myproduct');
					container.appendChild(selectList);

					//Create and append the options
					for (var j = 0; j < pause.length; j++) {
					    var option = document.createElement("option");
					    option.value = pause[j];
					    option.text = pause[j];
					    selectList.appendChild(option);
					}
					

	            	//For Quantity

	                var input = document.createElement("input");
	                input.type = "text";
	                input.setAttribute('name','quantity[]');
	                input.setAttribute('id','quantity[]');
	                input.setAttribute('class','myquantity ');
	                input.setAttribute('placeholder','Quantity');
	                container.appendChild(input);
	                
	                var input2 = document.createElement("input");
	                input2.type = "text";
	                input2.setAttribute('name','price[]');
	                input2.setAttribute('id','price[]');
	                input2.setAttribute('class','myprice ');
	                input2.setAttribute('placeholder','Price');                
	                container.appendChild(input2);
	                container.appendChild(document.createElement("br"));
                
           } 
                document.getElementById('submit').style.display = 'block';
                document.getElementById('discount').style.display = 'block';



                document.getElementById('calculate').innerHTML="Calculate";
                document.getElementById('calculate').style.display = 'block';
                document.getElementById('header').style.display = 'block';
                document.getElementById('filldetails').style.display = 'none';



        }   
    </script>
</head>

<body>
	<?php  include("navigationbar.php"); ?>
	
	 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
	<script>
		
	</script>
	


	<form action="processtransaction.php" method="post" id="myform" onsubmit="return doValidation();">
		<div class="container-fluid">
		<div class="row">	
			<div class="card col-lg-10 offset-lg-1">
				<div class="card-header"><h4><center>Place Order</center></h4></div>
			<div class="card-body">	

			<div class="container">	
			    


			

			     <div class="row">
			    	<div class="col-lg-3 offset-lg-1 input-group ">
			    		<input type="text" class="form-control" placeholder="Vendor name" aria-label="Number of Receipents" name="vendor_name" id="vendor_name">
			    	</div>
			    
			    	<div class="col-lg-3 input-group">
			    		<input type="text" class="form-control" placeholder="YYYY-MM-DD" aria-label="Number of Receipents" name="order_date" id="order_date">
			    	</div>
			

			  






			    	<div class="col-lg-3 input-group">
			    		<input type="text" class="form-control" placeholder="Number of products" aria-label="Number of Receipents" name="member" id="member">
			    	</div>
			    </div><br>

			    <div class="row">
			    	<div class="col-lg-9 offset-lg-1 ">
			    		<div id="container"/>
			    	</div>
			    </div>
		    </div>

		    

		    <div class="container">  
			      <div class="row">
				      	<div class="col-lg-2 offset-lg-1">
				    		<button style="display: none" type="button" class="btn btn-primary btn-lg" value="Calculate" id="calculate" name="calculate" onclick="calculatePrice()">Calculate</button>
					
				    	</div>

				    	<div class="col-lg-3 ">

				    		<p id="header" name="header"></p>
							<!--<input type="submit" value="submit" name="submit"></input> -->
				    	</div>

				    	<div class="col-lg-3 offset-lg-1 input-group">
				    		<input style="display: none" type="text" class="form-control" placeholder="Discount" aria-label="Number of Receipents" name="discount" id="discount">
				    	</div>
			     </div><br><br>

			    

			    <div class="row">
			    	<div class="col-lg-3 offset-lg-1">
			    		<button type="button" class="btn btn-primary btn-lg" value="Fill Details" id="filldetails" name="filldetails" onclick="addFields()">Fill details</button>
				
			    	</div>

			    	<div class="col-lg-2 ">
			    		<button style="display: none" id="submit" type="submit" name="submit" class="btn btn-primary btn-lg">Submit</button>
						<!--<input type="submit" value="submit" name="submit"></input> -->
			    	</div>
			    </div><br>

			 	</div>
			</div>
			</div>
		</div>	
		</div>
	</form>
	

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

</body>
</html>