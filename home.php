<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-2 offset-lg-1">
			<div class="card">
			  <h5 class="card-header">Category</h5>
			  <div class="card-body">
			    <h5 class="card-title">Manage Category</h5>
			    
			    <a href="managecategory.php" class="btn btn-primary">Go</a>
			  </div>
			</div>

		</div>


		<div class="col-lg-2 offset-lg-1">
			<div class="card">
			  <h5 class="card-header">Products</h5>
			  <div class="card-body">
			    <h5 class="card-title">Manage Products</h5>
			    
			    <a href="manageproduct.php" class="btn btn-primary">Go</a>
			  </div>
			</div>

		</div>


		<div class="col-lg-2 offset-lg-1">
			<div class="card">
			  <h5 class="card-header">Transactions</h5>
			  <div class="card-body">
			    <h5 class="card-title">Manage Transactions</h5>
			    
			    <a href="managetransaction.php" class="btn btn-primary">Go</a>
			  </div>
			</div>

		</div>
	</div>	
</div>
		


	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>