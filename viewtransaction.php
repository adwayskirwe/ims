<?php session_start();
include("connection.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<title>View Transactions</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

</head>
<body>
	<?php include("navigationbar.php"); ?>

	<script type="text/javascript">
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
	</script>


	<?php 
		$sql = "select * from transaction_ims";
		if($resultset = mysqli_query($conn, $sql)){
			
			echo "	<h4><center>Transactions</center></h4>
					<div class='tab-pane fade show' id='home1' role='tabpanel' aria-labelledby='home1-tab'>
					<div class='container'>
						<table id='myTable' class='table table-responsive table-hover'>
							<thead class='thead-light'>
							 
								<tr>
									<th scope='col'>Transaction ID</th>
									<th scope='col'>Vendor name</th>
									<th scope='col'>Order Date</th>
									<th scope='col'>Sub total</th>
									<th scope='col'>Discount offered</th>
									<th scope='col'>Net total</th>
									<th scope='col'>View Details</th>
									
								</tr>
							
							</thead>
							<tbody>";
					while($row = mysqli_fetch_array($resultset)){
						//to get document status and created by

						
							echo"<tr>
								<td><a href='viewdetails.php?tid=$row[0]''>$row[0]</td> 
								<td>$row[1]</td>
								<td>$row[2]</td> 
								<td>$row[3]</td>
								<td>$row[4]</td>
								<td>$row[5]</td>
								<td><a href='#viewModal' class='btn btn-info btn-lg' data-toggle='modal' data-edit_product_id='$row[0]' data-edit_product_name='$row[1]' data-edit_product_category='$row[2]' data-edit_product_quantity='$row[3]'> View</a></td>
								
								
								";
					}
					echo"</tbody>
						</table>
					 </div></div>";
		}

	?>


	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</body>
</html>