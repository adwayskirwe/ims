<?php session_start();
include("connection.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  


</head>
<body>
 <?php include('navigationbar.php'); ?>

	<script type="text/javascript">
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
	</script>


    <script type="text/javascript">
    	function editvalidate(){
    		var qty = document.getElementById('edit_product_quantity').value;
    		var nam = document.getElementById('edit_product_name').value;
    		var cat = document.getElementById('edit_product_category').value;
    		if(qty.length ==0 || nam.length ==0 || cat.length == 0 ){
    			window.alert('please fill all details');
    			return false;
    		}
    		if(isNaN(qty)){
    			window.alert('quantity must be a number1');
    			return false;
    		}
          return true;
    	}

       function addvalidate(){
    		var qty = document.getElementById('product_quantity').value;
    		var nam = document.getElementById('product_name').value;
    		var cat = document.getElementById('product_category').value;
    		if(qty.length ==0 || nam.length ==0 || cat.length == 0 ){
    			window.alert('please fill all details');
    			return false;
    		}
    		if(isNaN(qty)){
    			window.alert('quantity must be a number2');
    			return false;
    		}
          return true;
    	}
    </script>


	<!--Display all category-->
	<?php $sql = "select * from product";
		if($resultset = mysqli_query($conn, $sql)){
					echo "
					<div class='tab-pane fade show' id='home1' role='tabpanel' aria-labelledby='home1-tab'>
					<div class='container'><div class='row'><div class='col-lg-9 offset-lg-1'>
						<table id='myTable' class='table table-responsive table-hover'>
							<thead class='thead-light'>
								<tr>
									<th scope='col'>Product ID</th>
									<th scope='col'>Product name</th>
									<th scope='col'>Product category</th>
									<th scope='col'>Product quantity</th>
									<th scope='col'>Edit</th>
									<th scope='col'>Delete</th>
									
								</tr>
							</thead>
							<tbody>";
					while($row = mysqli_fetch_array($resultset)){
						//to get document status and created by
						
						
							echo"<tr>
								<td>$row[0]</td> 
								<td>$row[1]</td>
								<td>$row[2]</td> 
								<td>$row[3]</td>
								<td><a href='#editModal' class='btn btn-info btn-lg' data-toggle='modal' data-edit_product_id='$row[0]' data-edit_product_name='$row[1]' data-edit_product_category='$row[2]' data-edit_product_quantity='$row[3]'> Edit</a></td>
								<td><a href='#deleteModal' class='btn btn-info btn-lg' data-toggle='modal' data-delete_product_id='$row[0]' data-delete_product_name='$row[1]' data-delete_product_category='$row[2]'  data-delete_product_quantity='$row[3]'> Delete</a></td>
								
								";
					}
					echo"</tbody>
						</table></div></div>
					 </div></div>";
				}
				else{
					echo mysqli_error($conn);
				}


	?>




	<!-- ADD CATEGORT MODAL TRIGGER  -->
<br>
<div class="container">
	<div class="row">
		<div class="col-lg-4 offset-lg-4">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
			  Add Product
			</button>
		</div>
	</div>
</div>

<!-- MODAL FOR ADD CATEGORY -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">Add Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="post" action="processproduct.php" onsubmit="return addvalidate();">
	      <div class="modal-body">
	        	<!--FORM GOES HERE-->
	        	
					  <div class="form-group">
						    <label for="exampleInputEmail1">Product Name</label>
						    <input name="product_name" type="text" class="form-control" id="product_name" aria-describedby="emailHelp" placeholder="Enter product name">
						    <label for="exampleInputEmail1">Product Category</label>
						    <input name="product_category" type="text" class="form-control" id="product_category" aria-describedby="emailHelp" placeholder="Enter product quantity">
						    <label for="exampleInputEmail1">Product Quantity</label>
						    <input name="product_quantity" type="text" class="form-control" id="product_quantity" aria-describedby="emailHelp" placeholder="Enter product quantity">
						    


						    

					  </div>
		 </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <input id="add_product_button" value="Add" name="add_product_button" type="submit" class="btn btn-primary"></input>
	      </div>
	   </form>   
    </div>
  </div>
</div>
<!--MODAL FOR EDIT CATEGORY -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="post" action="processproduct.php" onsubmit="return editvalidate();">
	      <div class="modal-body">
	        	<!--FORM GOES HERE-->
	        	
					  <div class="form-group">
					  		<label for="new_category_name">Product Id</label>
						    <input name="edit_product_id" type="text" class="form-control" id="edit_product_id" aria-describedby="emailHelp"  readonly/>
						    <label for="exampleInputEmail1">Product Name</label>
						    <input name="edit_product_name" type="text" class="form-control" id="edit_product_name" aria-describedby="emailHelp" placeholder="Enter product name">
						    <label for="exampleInputEmail1">Product Category</label>
						    <input name="edit_product_category" type="text" class="form-control" id="edit_product_category" aria-describedby="emailHelp" placeholder="Enter product quantity">
						    <label for="exampleInputEmail1">Product Quantity</label>
						    <input name="edit_product_quantity" type="text" class="form-control" id="edit_product_quantity" aria-describedby="emailHelp" placeholder="Enter product quantity">
						    
						    
					  </div>
		 </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <input id="edit_product_button" value="Edit" name="edit_product_button" type="submit" class="btn btn-primary"></input>
	      </div>
	   </form>   
    </div>
  </div>
</div>



<script type="text/javascript">
	$(function () {
  $('#editModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var edit_product_id = button.data('edit_product_id'); // Extract info from data-* attributes
    var edit_product_name = button.data('edit_product_name');
    var edit_product_category = button.data('edit_product_category');
    var edit_product_quantity = button.data('edit_product_quantity'); // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#edit_product_id').val(edit_product_id);
    modal.find('#edit_product_name').val(edit_product_name);
    modal.find('#edit_product_category').val(edit_product_category);
    modal.find('#edit_product_quantity').val(edit_product_quantity);
  });
});
</script>


<!--MODAL FOR DELETE CATEGORY -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="post" action="processproduct.php">
	      <div class="modal-body">
	        	<!--FORM GOES HERE-->
	        	
					  <div class="form-group">
					  		<label for="new_category_name">Product Id</label>
						    <input name="delete_product_id" type="text" class="form-control" id="delete_product_id" aria-describedby="emailHelp"  readonly/>
						    <label for="exampleInputEmail1">Product Name</label>
						    <input name="delete_product_name" type="text" class="form-control" id="delete_product_name" aria-describedby="emailHelp" placeholder="Enter product name" readonly/>
						    <label for="exampleInputEmail1">Product Category</label>
						    <input name="delete_product_category" type="text" class="form-control" id="delete_product_category" aria-describedby="emailHelp" placeholder="Enter product quantity" readonly/>
						    <label for="exampleInputEmail1">Product Quantity</label>
						    <input name="delete_product_quantity" type="text" class="form-control" id="delete_product_quantity" aria-describedby="emailHelp" placeholder="Enter product quantity" readonly/>
						   
					  </div>
		 </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <input id="delete_product_button" value="Delete" name="delete_product_button" type="submit" class="btn btn-primary"></input>
	      </div>
	   </form>   
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function () {
  $('#deleteModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var delete_product_id = button.data('delete_product_id'); // Extract info from data-* attributes
    var delete_product_name = button.data('delete_product_name');
    var delete_product_category = button.data('delete_product_category');
    var delete_product_quantity = button.data('delete_product_quantity'); // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find('#delete_product_id').val(delete_product_id);
    modal.find('#delete_product_name').val(delete_product_name);
    modal.find('#delete_product_category').val(delete_product_category);
    modal.find('#delete_product_quantity').val(delete_product_quantity);
  });
});
</script>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

</body>
</html>